import 'package:demo_api/Model/UserLaptop.dart';
import 'package:demo_api/rest_client.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
class AddNewLaptop extends StatefulWidget {
  List<dynamic> list=[];
   String id="";
  AddNewLaptop({list,id}){
    this.id=id.toString();
    this.list=list;
  }
  @override
  State<AddNewLaptop> createState() => _AddNewLaptopState();
}

class _AddNewLaptopState extends State<AddNewLaptop> {
  List<TextEditingController> _controller =
      List.generate(8, (i) => TextEditingController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.id==""){
      for (int i = 0; i <= 7; i++) {_controller[i].text = "";};
    }
    else{
      for (int i = 0; i <= 7; i++) {_controller[i].text = widget.list[i].toString();}
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("ADD NEW Laptop"),
          backgroundColor: Colors.black,
        ),
        body:  bodycontaint(widget.id, _controller, context)
    );
  }

}

Widget CustomForFormField({required controller, required i}) {
  List<Map<int, String>> _text = [
    {
      0: 'Laptop Price (int) ',
      1: 'Laptop Name ',
      2: 'Laptop Image',
      3: 'Laptop RAM (int)',
      4: 'Laptop HD',
      5: 'Laptop Proccesser ',
      6: 'Laptop GraphicsCard (int)',
      7: 'id'
    }
  ];
  return TextFormField(
    controller: controller,
    keyboardType: TextInputType.number,
    decoration: InputDecoration(
      hintText: _text[0][i],
      border: const OutlineInputBorder(
        borderSide: BorderSide(width: 1, color: Colors.black),
      ),
    ),
  );
}



bodycontaint(id,_controller,context){
  if(id!=""){
    return Column(
      children: [
        for (int i = 0; i < _controller.length; i++)
          CustomForFormField(controller: _controller[i], i: i),
        TextButton(
          onPressed: () {
            UserLaptop laptop = UserLaptop(
                laptopPrice: int.parse(_controller[0].text),
                laptopName: _controller[1].text.toString(),
                laptopImage: _controller[2].text.toString(),
                laptopRAM: int.parse(_controller[3].text),
                laptopHD: _controller[4].text.toString(),
                laptopProcessor: _controller[5].text.toString(),
                laptopGraphicsCard: int.parse(_controller[6].text),
                id: _controller[7].text.toString());
            editLaptop(laptop: laptop, id: id.toString()).then((value) => Navigator.pop(context));
          },
          child: Text(
            "Edit",
            style: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
        ),
      ],
    );
  }
  return Column(
    children: [
      for (int i = 0; i < _controller.length; i++)
        CustomForFormField(controller: _controller[i], i: i),
      TextButton(
        onPressed: () async {
          addLaptop(
              laptopPrice: _controller[0].text,
              laptopName: _controller[1].text,
              laptopImage: _controller[2].text,
              laptopRAM: _controller[3].text,
              laptopHD: _controller[4].text,
              laptopProcessor: _controller[5].text,
              laptopGraphicsCard: _controller[6].text,
              id: _controller[7].text).then((value) => Navigator.pop(context));
        },
        child: Text(
          "ADD",
          style: TextStyle(
              color: Colors.black,
              fontSize: 20,
              fontWeight: FontWeight.bold),
        ),
      ),
    ],
  );
}


Future<void> addLaptop(
    {required laptopPrice,
      required laptopName,
      required laptopImage,
      required laptopRAM,
      required laptopHD,
      required laptopProcessor,
      required laptopGraphicsCard,
      required id}) async {
  UserLaptop laptop = UserLaptop(
      id: id.toString(),
      laptopGraphicsCard: int.parse(laptopGraphicsCard),
      laptopHD: laptopHD.toString(),
      laptopImage: laptopImage.toString(),
      laptopName: laptopName.toString(),
      laptopPrice: int.parse(laptopPrice),
      laptopProcessor: laptopProcessor.toString(),
      laptopRAM: int.parse(laptopRAM));
  final dio = Dio();
  final client = RestClient(dio);
  await client.createTask(laptop).then((value) {
    getLaptop();
    },);
  }
