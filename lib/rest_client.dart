import 'dart:convert';
import 'dart:js';
import 'package:demo_api/Model/UserLaptop.dart';
import 'package:demo_api/Model/user_laptop_model.dart';
import 'package:demo_api/add_laptop.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: "https://630ce2de53a833c53437ab57.mockapi.io/")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET("/Laptops")
  Future<String> getTasks();

  @GET("/Laptops/{id}")
  Future<UserLaptop> getTaskId(@Path("id") String id);

  @POST("/Laptops")
  Future<UserLaptop> createTask(@Body() UserLaptop task);

  @DELETE("/Laptops/{id}")
  Future<void> deleteTask(@Path("id") String id);

  @PUT("/Laptops/{id}")
  Future<UserLaptop> updateTask(@Path("id") String id, @Body() UserLaptop task);
}

class Myapp extends StatefulWidget {
  const Myapp({Key? key}) : super(key: key);

  @override
  State<Myapp> createState() => _MyappState();
}

class _MyappState extends State<Myapp> {

 late List<dynamic> listofdata;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("List Of Laptop"),
        backgroundColor: Colors.black,
        actions: [
          TextButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AddNewLaptop(list: [],id:""),
                  )).then((value) {
                    setState(() {
                      getLaptop();
                    });
              });
            },
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
          ),
        ],
      ),
      body: FutureBuilder<LaptopModel>(
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemBuilder: (context, index) {
                return Card(
                  child: Row(
                    children: [
                      Text(
                          snapshot.data!.resultList![index].laptopName
                              .toString(),
                          style: GoogleFonts.getFont('Lato', fontSize: 20)),
                      Spacer(),
                      TextButton(
                        onPressed: () async {
                          final dio = Dio();
                          final client = RestClient(dio);
                          UserLaptop data = await client.getTaskId(
                            snapshot.data!.resultList![index].id.toString(),
                          );
                          listofdata = [
                            data.laptopPrice,
                            data.laptopName,
                            data.laptopImage,
                            data.laptopRAM,
                            data.laptopHD,
                            data.laptopProcessor,
                            data.laptopGraphicsCard,
                            data.id,
                          ];
                          Navigator.push(context, MaterialPageRoute(builder: (context) => AddNewLaptop(list: listofdata,id: snapshot.data!.resultList![index].id!.toString()),)).then((value) {
                            setState(() {
                              getLaptop();
                            });
                          });
                        },
                        child: Icon(
                          Icons.add,
                          color: Colors.orange,
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          deleteLaptop(
                              id: snapshot.data!.resultList![index].id);
                        },
                        child: Icon(Icons.delete, color: Colors.red),
                      ),
                    ],
                  ),
                );
              },
              itemCount: snapshot.data!.resultList!.length,
            );
          } else {
            return CircularProgressIndicator();
          }
        },
        future: getLaptop(),
      ),
    );
  }

  Future<void> deleteLaptop({required id}) async {
    final dio = Dio();
    final client = RestClient(dio);
    await client.deleteTask(id.toString());
    setState(() {
      getLaptop();
    });
  }
}

Future<LaptopModel> getLaptop() async {
  final dio = Dio();
  final client = RestClient(dio);
  String data = await client.getTasks();
  LaptopModel data1 = LaptopModel.fromJson(json.decode(data));
  return data1;
}

Future<void> editLaptop({required UserLaptop laptop,required id}) async {
  final dio = Dio();
  final client = RestClient(dio);
  await client.updateTask(id.toString(), laptop).then((value) {
  });
}
