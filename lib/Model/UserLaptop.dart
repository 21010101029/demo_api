import 'dart:convert';

UserLaptop userLaptopFromJson(String str) =>
    UserLaptop.fromJson(json.decode(str));

String userLaptopToJson(UserLaptop data) => json.encode(data.toJson());

class UserLaptop {
  UserLaptop({
    this.laptopPrice,
    this.laptopName,
    this.laptopImage,
    this.laptopRAM,
    this.laptopHD,
    this.laptopProcessor,
    this.laptopGraphicsCard,
    this.id,
  });

  UserLaptop.fromJson(dynamic json) {
    laptopPrice = json['LaptopPrice'];
    laptopName = json['LaptopName'];
    laptopImage = json['LaptopImage'];
    laptopRAM = json['LaptopRAM'];
    laptopHD = json['LaptopHD'].toString();
    laptopProcessor = json['LaptopProcessor'];
    laptopGraphicsCard = json['LaptopGraphicsCard'];
    id = json['id'];
  }

  num? laptopPrice;
  String? laptopName;
  String? laptopImage;
  num? laptopRAM;
  String? laptopHD;
  String? laptopProcessor;
  num? laptopGraphicsCard;
  String? id;

  UserLaptop copyWith({
    num? laptopPrice,
    String? laptopName,
    String? laptopImage,
    num? laptopRAM,
    String? laptopHD,
    String? laptopProcessor,
    num? laptopGraphicsCard,
    String? id,
  }) =>
      UserLaptop(
        laptopPrice: laptopPrice ?? this.laptopPrice,
        laptopName: laptopName ?? this.laptopName,
        laptopImage: laptopImage ?? this.laptopImage,
        laptopRAM: laptopRAM ?? this.laptopRAM,
        laptopHD: laptopHD ?? this.laptopHD,
        laptopProcessor: laptopProcessor ?? this.laptopProcessor,
        laptopGraphicsCard: laptopGraphicsCard ?? this.laptopGraphicsCard,
        id: id ?? this.id,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['LaptopPrice'] = laptopPrice;
    map['LaptopName'] = laptopName;
    map['LaptopImage'] = laptopImage;
    map['LaptopRAM'] = laptopRAM;
    map['LaptopHD'] = laptopHD;
    map['LaptopProcessor'] = laptopProcessor;
    map['LaptopGraphicsCard'] = laptopGraphicsCard;
    map['id'] = id;
    return map;
  }
}
