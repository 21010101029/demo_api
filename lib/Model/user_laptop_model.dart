import 'package:demo_api/Model/UserLaptop.dart';

class LaptopModel{
  List<UserLaptop>? _resultList;
  LaptopModel({
    List<UserLaptop>? resultList,
  }){
    _resultList=resultList;
  }
  List<UserLaptop>? get resultList =>_resultList;
  LaptopModel.fromJson(var json){
    if(json!=null){
      _resultList=[];
      json.toList().forEach((v){
        // print('DATA ABC:::$v');
        _resultList?.add(UserLaptop.fromJson(v));
      });
    }
  }
}